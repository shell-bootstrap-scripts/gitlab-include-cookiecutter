import ruyaml

def read_project_branches_to_trigger():
  yaml = ruyaml.YAML()
  with open('project_branches_to_trigger.yaml', 'r') as triggerFile:
    project_branches_to_trigger = yaml.load(triggerFile)
  if type(project_branches_to_trigger) is not ruyaml.comments.CommentedMap:
    raise TypeError(type(project_branches_to_trigger), project_branches_to_trigger)
  if 'project_branches_to_trigger' not in project_branches_to_trigger:
    raise ValueError(project_branches_to_trigger)
  return project_branches_to_trigger

def read_replay_yaml(replay_yaml_path='replay.yaml'):
  yaml = ruyaml.YAML()
  with open(replay_yaml_path, 'r') as replayFile:
    replay = yaml.load(replayFile)
  if 'cookiecutter' not in replay:
    raise ValueError(replay)
  cookiecutter = replay['cookiecutter']
  if type(cookiecutter) is not ruyaml.comments.CommentedMap:
    raise TypeError(type(cookiecutter), cookiecutter)
  if 'project_branches_to_trigger' not in cookiecutter:
    raise ValueError(cookiecutter)
  return replay
