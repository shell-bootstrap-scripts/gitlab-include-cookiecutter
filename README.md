# GitLab Include Cookiecutter

This cookiecutter is for any YAML file meant to be included in GitLab CI. But we cannot actually anticipate what might go on such a YAML file. But we can guess that, in the initial stages, when testing the includable, we'll be using it in some enumerable set of projects and branches.

This cookiecutter sets you up to *trigger* a set of projects and branches, one per job. We need a cookiecutter because GitLab only allows us to trigger one branch per job. The assumption is that those branches of those projects have been set up to include the includable, so testing them tests it.

The list of projects and branches to trigger is loaded from an external file. That's why we need cookiecut_script.sh.
